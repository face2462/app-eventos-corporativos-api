package com.example.exceptions;

import com.example.enums.ErrorEnum;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
@AllArgsConstructor
public class ApiException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	private ErrorEnum error;
    private String message;


}

