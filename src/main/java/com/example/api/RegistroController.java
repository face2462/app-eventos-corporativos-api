package com.example.api;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.business.RegistroBusiness;
import com.example.dto.RegistroRequestDTO;
import com.example.dto.RegistroResponseDTO;
import com.example.model.RegistroEntity;
import com.example.util.MapperUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "Accion exitosa"),
        @ApiResponse(code = 500, message = "Internal Server Error")
})
@Api(value = "app-eventos-corporativos-api RegistroController", 
description = "Registro de personas a eventos que se van a realizar.", 
	tags = "Registros")
public class RegistroController {
	
	RegistroBusiness registroBusiness;
	
	@ApiOperation(value = "Obtener lista de Registro", notes = "Retorna listado de Registro del sistema xxxxx")
	@GetMapping("registro")
    public List<RegistroResponseDTO> getAll() {
       return MapperUtil.mapAll(registroBusiness.getAll(),RegistroResponseDTO.class);
    }

	@ApiOperation(value = "Guarda o Edita Registra Personas a Eventos", 
			notes = "Retorna el registro de personas a eventos guardado o editado.")
	@PostMapping("registro")
    public ResponseEntity<RegistroRequestDTO> saveOrUpdate(@RequestBody RegistroRequestDTO dto) {
		RegistroEntity data = MapperUtil.map(dto,RegistroEntity.class);
		registroBusiness.saveOrUpdate(data);
        return new ResponseEntity<RegistroRequestDTO>(dto, HttpStatus.OK);
    }
}
