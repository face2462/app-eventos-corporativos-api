package com.example.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.business.EventoBusiness;
import com.example.dto.EventoRequestDTO;
import com.example.dto.EventoResponseDTO;
import com.example.model.EventoEntity;
import com.example.util.MapperUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "Accion exitosa"),
        @ApiResponse(code = 500, message = "Internal Server Error")
})
@Api(value = "app-eventos-corporativos-api EventoController", 
description = "Eventos corporativos que se van a realizar.", 
	tags = "Eventos")
public class EventoController {
	
	@Autowired
	EventoBusiness eventoBusiness;
	
	@ApiOperation(value = "Obtener lista de Evento", notes = "Retorna listado de Evento del sistema xxxxx")
	@GetMapping("evento")
    public List<EventoResponseDTO> getAll() {
       return MapperUtil.mapAll(eventoBusiness.getAll(),EventoResponseDTO.class);
    }

	@ApiOperation(value = "Guarda o Edita Eventos", notes = "Retorna el evento guardado o editado.")
	@PostMapping("evento")
    public ResponseEntity<EventoRequestDTO> saveOrUpdate(@RequestBody EventoRequestDTO dto) {
		EventoEntity data = MapperUtil.map(dto,EventoEntity.class);
		eventoBusiness.saveOrUpdate(data);
        return new ResponseEntity<EventoRequestDTO>(dto, HttpStatus.OK);
    }
}
