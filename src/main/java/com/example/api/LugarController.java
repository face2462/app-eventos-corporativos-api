package com.example.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.business.LugarBusiness;
import com.example.dto.LugarRequestDTO;
import com.example.dto.LugarResponseDTO;
import com.example.model.LugarEntity;
import com.example.util.MapperUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "Accion exitosa"),
        @ApiResponse(code = 500, message = "Internal Server Error")
})
@Api(value = "app-eventos-corporativos-api LugarController", 
description = "Lugar y capacidad donde se realizaran los eventos corporativos", 
	tags = "Lugares")
public class LugarController {
	
	@Autowired
    LugarBusiness lugarBusiness;
	
	@ApiOperation(value = "Obtener lista de Lugar", notes = "Retorna listado de Lugar del sistema.")
	@GetMapping("lugar")
    public List<LugarResponseDTO> getAll() {
       return MapperUtil.mapAll(lugarBusiness.getAll(),LugarResponseDTO.class);
    }
	
	@ApiOperation(value = "Guarda o Edita Lugares", notes = "Retorna el lugar guardado o editado.")
	@PostMapping("lugar")
    public ResponseEntity<LugarRequestDTO> saveOrUpdate(@RequestBody LugarRequestDTO dto) {
		LugarEntity data = MapperUtil.map(dto,LugarEntity.class);
		lugarBusiness.saveOrUpdate(data);
        return new ResponseEntity<LugarRequestDTO>(dto, HttpStatus.OK);
    }
}
