package com.example.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.model.EventoEntity;
import com.example.repository.EventoRepository;
import com.example.util.LoggerUtil;

@Component
public class EventoBusiness {
	
	@Autowired
	LoggerUtil log;
	
	@Autowired
    EventoRepository eventoRepository;

	public void saveOrUpdate(EventoEntity data) {
		eventoRepository.save(data);
	}

	public List<EventoEntity> getAll() {
		List<EventoEntity> result = new ArrayList<EventoEntity>();
		eventoRepository.findAll().forEach((final EventoEntity r) -> result.add(r));
		return result;
	}
}
