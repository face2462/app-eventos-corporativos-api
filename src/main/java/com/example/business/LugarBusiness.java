package com.example.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.model.LugarEntity;
import com.example.repository.LugarRepository;
import com.example.util.LoggerUtil;

@Component
public class LugarBusiness {

	@Autowired
	LoggerUtil log;
	
	@Autowired
    LugarRepository lugarRepository;

	public void saveOrUpdate(LugarEntity data) {
		lugarRepository.save(data);
	}

	public List<LugarEntity> getAll() {
		List<LugarEntity> result = new ArrayList<LugarEntity>();
		lugarRepository.findAll().forEach((final LugarEntity r) -> result.add(r));
		return result;
	}
}
