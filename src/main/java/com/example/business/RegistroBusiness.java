package com.example.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.model.RegistroEntity;
import com.example.repository.RegistroRepository;
import com.example.util.LoggerUtil;

@Component
public class RegistroBusiness {
	
	@Autowired
	LoggerUtil log;
	
	@Autowired
    RegistroRepository registroRepository;

	public void saveOrUpdate(RegistroEntity data) {
		registroRepository.save(data);
	}

	public List<RegistroEntity> getAll() {
		List<RegistroEntity> result = new ArrayList<RegistroEntity>();
		registroRepository.findAll().forEach((final RegistroEntity r) -> result.add(r));
		return result;
	}
}
