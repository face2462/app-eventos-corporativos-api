package com.example.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Evento")
@Entity
public class EventoEntity  implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@GeneratedValue( generator = "uuid2" )
    @GenericGenerator( name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator" )
    @Column( name = "id", columnDefinition = "VARCHAR(36)" )
	@Id
    private String id;

    private String nombre;
    private Date fecha;
    private String descripcion;
    
    @JoinTable(
         name = "EventoLugar",
         joinColumns = @JoinColumn(name = "evento_id", nullable = false),
         inverseJoinColumns = @JoinColumn(name="lugar_id", nullable = false)
    )
    @ManyToMany(cascade = CascadeType.MERGE)
    private List<LugarEntity> lugar;
    
    private boolean estado;
    
    public void addLugar(LugarEntity lugar){
        if(this.lugar == null){
            this.lugar = new ArrayList<>();
        }
        
        this.lugar.add(lugar);
    }
    
    @JsonIgnore
    @OneToMany(mappedBy="evento")
    private List<RegistroEntity> registro;
}
